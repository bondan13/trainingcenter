<div class="left-side sticky-left-side">

    <!--logo and iconic logo start-->
    <div class="logo">
        <a href="<?=Yii::app()->getBaseUrl(true)?>/admin"><img src="<?=Yii::app()->getBaseUrl(true)?>/images/gt.gif" height="50" /></a>
    </div>

    <div class="logo-icon text-center">
        <a href="<?=Yii::app()->getBaseUrl(true)?>/admin"><img src="<?=Yii::app()->getBaseUrl(true)?>/images/gts.jpg" height="42" /></a>
    </div>
    <!--logo and iconic logo end-->


    <div class="left-side-inner">
        
        <!--sidebar nav start-->
        <ul class="nav nav-pills nav-stacked custom-nav">
            
            <li class="menu-list<?=($this->getUniqueId()=='admin/departemen')?' nav-active':''?>"><a href="#"><i class="fa fa-sitemap"></i> <span>Departemen</span></a>
                <ul class="sub-menu-list">
                    <li><a href="<?=Yii::app()->getBaseUrl(true)?>/admin/departemen">ListDepartemen</a></li>
                </ul>
            </li>

            <li class="menu-list<?=($this->getUniqueId()=='admin/pegawai')?' nav-active':''?>"><a href=""><i class="fa fa-users"></i> <span>Pegawai</span></a>
                <ul class="sub-menu-list">
                    <li><a href="<?=Yii::app()->getBaseUrl(true)?>/admin/pegawai">List Pegawai</a></li>
                </ul>
            </li>

            <li class="menu-list<?=($this->getUniqueId()=='admin/itemtraining')?' nav-active':''?>"><a href=""><i class="fa fa-tachometer"></i> <span>Materi Training</span></a>
                <ul class="sub-menu-list">
                    <li><a href="<?=Yii::app()->getBaseUrl(true)?>/admin/itemtraining">List Materi Training</a></li>
                </ul>
            </li>
            
            <li class="menu-list<?=($this->getUniqueId()=='admin/jadwal')?' nav-active':''?>"><a href=""><i class="fa fa-calendar"></i> <span>Jadwal Training</span></a>
                <ul class="sub-menu-list">
                    <li><a href="<?=Yii::app()->getBaseUrl(true)?>/admin/jadwal">List Jadwal</a></li>
                    <li><a href="<?=Yii::app()->getBaseUrl(true)?>/admin/jadwal/create">Buat Jadwal</a></li>
                </ul>
            </li>

            <li class="menu-list<?=($this->getUniqueId()=='admin/training')?' nav-active':''?>"><a href=""><i class="fa fa-th-list"></i> <span>Training</span></a>
                <ul class="sub-menu-list">
                    <li><a href="<?=Yii::app()->getBaseUrl(true)?>/admin/training">Validasi Peserta Training </a></li>
                    <li><a href="<?=Yii::app()->getBaseUrl(true)?>/admin/training/laporan">Laporan Training </a></li>
                </ul>
            </li>

            <li><a href="<?=Yii::app()->getBaseUrl(true)?>/site/logout"><i class="fa fa-sign-in"></i> <span>Keluar</span></a></li>

        </ul>
        <!--sidebar nav end-->

    </div>
</div>