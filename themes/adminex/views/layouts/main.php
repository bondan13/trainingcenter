<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ThemeBucket">
        <link rel="shortcut icon" href="#" type="image/png">

        <title> <?php echo Yii::app()->name; ?> </title>

        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style.css" rel="stylesheet">
        <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/style-responsive.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="sticky-header">

        <section>
            <!-- left side start-->
            <?php $this->renderPartial('//layouts/partial/kiri'); ?>
            <!-- left side end-->

            <!-- main content start-->
            <div class="main-content" >
                <!-- header section start-->
                <div class="header-section">

                    <!--toggle button start-->
                    <a class="toggle-btn">
                        <i class="fa fa-bars"></i>
                    </a>
                    <div class="menu-right">
                        <img src="<?=Yii::app()->getBaseUrl(true)?>/images/gt-radial.jpg" height="50" /> Training Center
                    </div>
                    <!--notification menu end -->

                </div>
                <!-- header section end-->



                <!--body wrapper start-->
                <div class="wrapper">
                    <?php echo $content; ?>
                </div>
                <!--body wrapper end-->

                <!--footer section start-->
                <footer class="sticky-footer">
                    2016 &copy; Rofi Supriatna
                </footer>
                <!--footer section end-->


            </div>
            <!-- main content end-->
        </section>

        <!-- Placed js at the end of the document so the pages load faster -->
<!--        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-1.10.2.min.js"></script>-->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-ui-1.9.2.custom.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/modernizr.min.js"></script>
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/jquery.nicescroll.js"></script>


        <!--common scripts for all pages-->
        <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/scripts.js"></script>

    </body>
</html>
