<?php

$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'training-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'jadwal_tanggal',
            'filter' => CHtml::activeTextField($model, 'jadwal_tanggal'),
            'value' => '@$data->jadwal->batas_pendaftaran',
            'sortable' => true,
            'htmlOptions' => array(
                'style' => 'width: 120px;'
            )
        ),
        array(
            'name' => 'pegawai_nip',
            'filter' => CHtml::activeTextField($model, 'pegawai_nip'),
            'value' => '@$data->pegawai->nip',
            'sortable' => true,
            'htmlOptions' => array(
                'style' => 'width: 100px;'
            )
        ),
        array(
            'name' => 'pegawai_nama',
            'filter' => CHtml::activeTextField($model, 'pegawai_nama'),
            'value' => '@$data->pegawai->nama',
            'sortable' => true,
        ),
        array(
            'name' => 'training_nama',
            'filter' => CHtml::activeTextField($model, 'training_nama'),
            'value' => '@$data->itemtraining->nama',
            'sortable' => true,
        ),
        array(
            'name' => 'status',
            'filter' => [1 => 'Pending', 2 => 'Dijadwalkan', 3=>'Lulus', 4=>'Gagal'],
            'value' => '$data->status()',
            'sortable' => true,
            'htmlOptions' => array(
                'style' => 'width: 80px;'
            )
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{edit}&nbsp;&nbsp;&nbsp;&nbsp;{delete}',
            'buttons' => array
                (
                'edit' => array
                    (
                    'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Edit'),
                    'label' => '<i class="fa fa-pencil"></i>',
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("admin/training/update/", array("id"=>$data->id))',
                ),
                'delete' => array
                    (
                    'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Hapus'),
                    'label' => '<i class="fa fa-trash-o"></i>',
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("admin/training/delete/", array("id"=>$data->id))',
                ),
            ),
            'htmlOptions' => array(
                'style' => 'width: 70px;'
            )
        ),
    ),
    //'pagerCssClass' => 'pagination',
    'pager' => array(
        'header' => '',
        //'footer'=>'</div>',
        'firstPageLabel' => 'Frist',
        'prevPageLabel' => 'Prev',
        'nextPageLabel' => 'Next',
        'lastPageLabel' => 'Last',
        'hiddenPageCssClass' => 'disabled',
        'internalPageCssClass' => '',
        //'cssFile' => '/bootstrap/css/bootstrap.css',
        //'firstPageCssClass'=>'',
        'selectedPageCssClass' => 'active',
        'htmlOptions' => array('class' => 'pagination'),
    ),
    'pagerCssClass' => 'pagersmall',
    'itemsCssClass' => 'table table-striped table-hover',
    'htmlOptions' => array(
    //'class' => 'table-responsive'
    ),
    'template' => '<center><h4><i class="fa fa-cubes"></i> TRAINING </h4></center><small>{summary}</small>{items}{pager}',
));
?>

