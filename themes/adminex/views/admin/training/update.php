<center><h3><i class="fa fa-cubes"></i>TRAINING</h3></center>

<div class="form">

    <p><b>NIP :</b> <?= $model->pegawai->nip ?></p>
    <p><b>Nama :</b> <?= $model->pegawai->nama ?></p>
    <p><b>Training :</b> <?= $model->itemtraining->nama ?></p>
    <p><b>Tanggal :</b> <?= $model->jadwal->batas_pendaftaran ?></p>

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'training-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'enableClientValidation' => true
    ));
    ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'hasil'); ?><?php echo $form->error($model, 'hasil'); ?>
        <?php echo $form->textField($model, 'hasil', array('size' => 60, 'maxlength' => 255, 'class' => 'form-control')); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'keterangan'); ?><?php echo $form->error($model, 'keterangan'); ?>
        <?php echo $form->textArea($model, 'keterangan', array('class' => 'form-control')); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'status'); ?><?php echo $form->error($model, 'status'); ?>
        <?php echo $form->dropDownList($model, 'status', ['1'=>'Pending','2'=>'Dijadwalkan','3'=>'Lulus','4'=>'Gagal'], array('class' => 'form-control')); ?>
    </div>
    <br />
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Tambahkan' : 'Simpan', ['class' => 'btn btn-success']); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->