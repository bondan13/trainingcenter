<?php
/* @var $this DepartemenController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Departemens',
);

$this->menu=array(
	array('label'=>'Create Departemen', 'url'=>array('create')),
	array('label'=>'Manage Departemen', 'url'=>array('admin')),
);
?>

<h1>Departemens</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
