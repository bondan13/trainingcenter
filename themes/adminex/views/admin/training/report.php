

<center><h3><i class="fa fa-book"></i> Laporan Training Pegawai</h3>

    <form method="POST">
        <input id="Report_datestart" type="hidden" name="Report[date_start]">
        <input id="Report_dateend" type="hidden" name="Report[date_end]">
        <center><code>Harus Diisi</code></center>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'tanggal1',
            'value' => '',
            //'model'=>$model,
            //'attribute'=>'tanggal',
            // additional javascript options for the date picker plugin
            'options' => array(
                'showAnim' => 'clip',
                'altFormat' => 'yy-mm-dd',
                'altField' => '#Report_datestart', //id hidden form = Formulir_tanggal
                'dateFormat' => 'd MM yy',
                //'showButtonPanel'=>true,
                'changeMonth' => true,
                'changeYear' => true,
            //'yearRange'=>'-80:-16',
            //'todayBtn'=>true,
            //'disabled'=>true,
            ),
            'htmlOptions' => array(
                //'class' => 'form-control',
                'placeholder' => '  Tanggal Awal'
            ),
        ));
        ?>
        <sup>s/d</sup>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'tanggal2',
            'value' => '',
            //'model'=>$model,
            //'attribute'=>'tanggal',
            // additional javascript options for the date picker plugin
            'options' => array(
                'showAnim' => 'clip',
                'altFormat' => 'yy-mm-dd',
                'altField' => '#Report_dateend', //id hidden form = Formulir_tanggal
                'dateFormat' => 'd MM yy',
                //'showButtonPanel'=>true,
                'changeMonth' => true,
                'changeYear' => true,
            //'yearRange'=>'-80:-16',
            //'todayBtn'=>true,
            //'disabled'=>true,
            ),
            'htmlOptions' => array(
                //'class' => 'form-control',
                'placeholder' => '  Tanggal Akhir'
            ),
        ));

        echo '<br /><code>' . $message . "</code>";
        ?>
        <br /><br />
        <center><code>Opsional</code></center>
        <div class="col-lg-4 col-lg-offset-3">
            Training<br />
            <select name="listtraining" class="form-control">
                <option value="0" selected>All</option>
                <?php
                if ($listtraining) {
                    foreach ($listtraining as $l) {
                        echo '<option value="' . $l->id . '">' . $l->nama . '</value>';
                    }
                }
                ?>
            </select>
        </div>
        <div class="col-lg-2">
            Status<br />
            <select name="status" class="form-control">
                <option value="0" selected>All</option>
                <option value="1">Pending</option>
                <option value="2">Dijadwalkan</option>
                <option value="3">Lulus</option>
                <option value="4">Gagal</option>
            </select>
        </div>
        <div class="col-lg-12">
            <br />
            <input type="submit" value="Lihat Laporan" class="btn btn-success btn-sm">
        </div>
    </form>

</center>
