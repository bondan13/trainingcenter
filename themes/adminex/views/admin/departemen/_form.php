<?php
/* @var $this DepartemenController */
/* @var $model Departemen */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'departemen-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'enableClientValidation' => true
    ));
    ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'nama'); ?><?php echo $form->error($model, 'nama'); ?>
    <?php echo $form->textField($model, 'nama', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
    </div>
    <br />
    <div class="row buttons">
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Tambahkan' : 'Simpan', ['class' => 'btn btn-success']); ?>
    </div>

<?php $this->endWidget(); ?>

</div><!-- form -->