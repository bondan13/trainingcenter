<?php
$list = CHtml::listData(Departemen::model()->findAll(), 'id', 'nama');
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'pegawai-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'enableClientValidation' => true
    ));
    ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'nip'); ?><?php echo $form->error($model, 'nip'); ?>
        <?php echo $form->textField($model, 'nip', array('size' => 60, 'maxlength' => 7, 'class' => 'form-control')); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'nama'); ?><?php echo $form->error($model, 'nama'); ?>
        <?php echo $form->textField($model, 'nama', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'departemen_id'); ?><?php echo $form->error($model, 'departemen_id'); ?>
        <?= $form->dropDownList($model, 'departemen_id', $list, array('class' => 'form-control')); ?> 
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'level_id'); ?><?php echo $form->error($model, 'level_id'); ?>
        <?= $form->dropDownList($model, 'level_id',['1'=>1,'2'=>2,'3'=>3,'4'=>4,'5'=>5,'6'=>6], array('class' => 'form-control')); ?> 
    </div>
    <br />
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Tambahkan' : 'Simpan', ['class' => 'btn btn-success']); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->