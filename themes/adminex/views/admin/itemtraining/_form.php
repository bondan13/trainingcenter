<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'itemtraining-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'enableClientValidation' => true
    ));
    ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'nama'); ?><?php echo $form->error($model, 'nama'); ?>
        <?php echo $form->textField($model, 'nama', array('size' => 60, 'maxlength' => 100, 'class' => 'form-control')); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'jenis'); ?><?php echo $form->error($model, 'jenis'); ?>
        <?php echo $form->dropDownList($model, 'jenis', array('Internal' => 'Internal', 'External' => 'External'), array('class' => 'form-control')); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'tujuan'); ?><?php echo $form->error($model, 'tujuan'); ?>
        <?php echo $form->textArea($model, 'tujuan', array('class' => 'form-control')); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'target'); ?><?php echo $form->error($model, 'target'); ?>
        <?php echo $form->textArea($model, 'target', array('class' => 'form-control')); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'level'); ?><?php echo $form->error($model, 'level'); ?>
        <?php echo $form->dropDownList($model, 'level', ['1'=>1,'2'=>2,'3'=>3,'4'=>4,'5'=>5,'6'=>6], array('class' => 'form-control')); ?>
    </div>
    <br />
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Tambahkan' : 'Simpan', ['class' => 'btn btn-success']); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->