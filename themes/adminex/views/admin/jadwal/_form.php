<?php
$list = CHtml::listData(ItemTraining::model()->findAll(), 'id', 'nama');
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'jadwal-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'enableClientValidation' => true
    ));
    ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'training_id'); ?><?php echo $form->error($model, 'training_id'); ?>
        <?= $form->dropDownList($model, 'training_id', $list, array('class' => 'form-control')); ?> 
    </div>
    <?php echo $form->labelEx($model, 'keterangan'); ?><?php echo $form->error($model, 'keterangan'); ?>
        <?php echo $form->textArea($model, 'keterangan', array('class' => 'form-control')); ?>
    <div class="row">
        <?php echo $form->labelEx($model, 'kuota'); ?><?php echo $form->error($model, 'kuota'); ?>
        <?= $form->textField($model, 'kuota', array('class' => 'form-control')); ?> 
    </div>
    <div class="row">
        <?php
        echo $form->labelEx($model, 'batas_pendaftaran');
        $form->error($model, 'batas_pendaftaran');
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'end-upload',
            'value' => @$model->batas_pendaftaran,
            //'model'=>$model,
            //'attribute'=>'date_onair',
            // additional javascript options for the date picker plugin
            'options' => array(
                'altFormat' => 'yy-mm-dd',
                'altField' => '#Jadwal_batas_pendaftaran', //id hidden form = Formulir_tanggal
                'dateFormat' => 'd MM yy',
                //'showButtonPanel'=>true,
                'changeMonth' => true,
                'changeYear' => true,
            //'yearRange'=>'-80:-16',
            //'todayBtn'=>true,
            //'disabled'=>true,
            ),
            'htmlOptions' => array(
                //'class' => 'form-control',
                'placeholder' => '  Batas pendaftaran',
                'class' => 'form-control',
            //'onchange' => 'js:alert($("#Competition_start_upload").val())'
            ),
        ));
        ?>
        <?php echo $form->hiddenField($model, 'batas_pendaftaran'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model, 'status'); ?><?php echo $form->error($model, 'status'); ?>
        <?= $form->dropDownList($model, 'status', [0 => 'Not Active', 1 => 'Active'], array('class' => 'form-control')); ?> 
    </div>
    <br />
    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Tambahkan' : 'Simpan', ['class' => 'btn btn-success']); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->