<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'jadwal-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'id',
            'filter' => CHtml::activeTextField($model, 'id'),
            'value' => '$data->id',
            'sortable' => true,
            'htmlOptions' => array(
                'style' => 'width: 50px;'
            )
        ),
        array(
            'name' => 'itemtraining',
            'filter' => CHtml::activeTextField($model, 'itemtraining'),
            'value' => '$data->itemtraining->nama',
            'sortable' => true,
        ),
        array(
            'name' => 'status',
            'filter' => [0=>'[0] Not Active', 1=>'[1] Active'],
            'value' => '$data->status',
            'sortable' => true,
            'htmlOptions' => array(
                'style' => 'width: 80px;'
            )
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{edit}&nbsp;&nbsp;&nbsp;&nbsp;{delete}',
            'buttons' => array
                (
                'edit' => array
                    (
                    'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Edit'),
                    'label' => '<i class="fa fa-pencil"></i>',
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("admin/jadwal/update/", array("id"=>$data->id))',
                ),
                'delete' => array
                    (
                    'options' => array('rel' => 'tooltip', 'data-toggle' => 'tooltip', 'title' => 'Hapus'),
                    'label' => '<i class="fa fa-trash-o"></i>',
                    'imageUrl' => false,
                    'url' => 'Yii::app()->createUrl("admin/jadwal/delete/", array("id"=>$data->id))',
                ),
            ),
            'htmlOptions' => array(
                'style' => 'width: 70px;'
            )
        ),
    ),
    //'pagerCssClass' => 'pagination',
    'pager' => array(
        'header' => '',
        //'footer'=>'</div>',
        'firstPageLabel' => 'Frist',
        'prevPageLabel' => 'Prev',
        'nextPageLabel' => 'Next',
        'lastPageLabel' => 'Last',
        'hiddenPageCssClass' => 'disabled',
        'internalPageCssClass' => '',
        //'cssFile' => '/bootstrap/css/bootstrap.css',
        //'firstPageCssClass'=>'',
        'selectedPageCssClass' => 'active',
        'htmlOptions' => array('class' => 'pagination'),
    ),
    'pagerCssClass' => 'pagersmall',
    'itemsCssClass' => 'table table-striped table-hover',
    'htmlOptions' => array(
    //'class' => 'table-responsive'
    ),
    'template' => '<center><h4><i class="fa fa-cubes"></i> JADWAL TRAINING </h4></center><small>{summary}</small>{items}{pager}',
));
?>
<div class="navbar-right">
    <a href="<?php echo CController::createUrl('jadwal/create'); ?>" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Tambah</a>
</div>
