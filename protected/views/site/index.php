<?php
/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
?>
<div class="col-lg-4 col-lg-offset-2">
    <center><h1>ACHIEVMENT</h1></center>
    <?php
    if ($training) {
        foreach ($training as $t) {
            $btn = 'btn-primary';
            $g = 'glyphicon-remove';
            if($t->status == 1){
                $btn = 'btn-default';
                $g = 'glyphicon-time';
            }
            if($t->status == 2){
                $btn = 'btn-primary';
                $g = 'glyphicon-ok';
            }
            if($t->status == 3){
                $btn = 'btn-success';
                $g = 'glyphicon-certificate';
            }
            if($t->status == 4){
                $btn = 'btn-warning';
                $g = 'glyphicon-remove';
            }
            ?>
            <button class="btn btn-block <?=$btn?> btn-lg" onclick="modalShow(this)" id="<?= $t->id ?>">
                <i class="glyphicon <?=$g?>" style="float:left;"></i> <?= substr($t->itemtraining->nama, 0, 35) ?> <?= (strlen($t->itemtraining->nama) > 35) ? '...' : '' ?>
            </button>
            <?php
        }
    }
    ?>
</div>
<div class="col-lg-4">
    <center><h1>AVAILABLE</h1></center>
    <?php
    if ($itraining) {
        foreach ($itraining as $t) {
            ?>
            <button class="btn btn-block btn-primary btn-lg" onclick="modalShowJadwal(this)" id="<?= $t->id ?>">
                <i class="glyphicon glyphicon-cloud-download" style="color: white; float: right;"></i> <?= substr($t->itemtraining->nama, 0, 35) ?> <?= (strlen($t->itemtraining->nama) > 35) ? '...' : '' ?>
            </button>
            <?php
        }
    }
    ?>
</div>

<div class="modal fade" id="modal-container-244164" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <center>
                    <h4 class="modal-title" id="myModalLabel">

                    </h4>
                    <code id="jenis"></code> | <code id="kuota"></code>
                </center>
            </div>
            <div class="modal-body">
                <b>Keterangan :</b> <p id="keterangan"></p>
                <hr />
                <b>Tujuan :</b> <p id="tujuan"></p>
                <b>Target :</b> <p id="target"></p>
            </div>
            <div class="modal-footer">
                <a id="follow" href="#" class="btn btn-primary">Ikuti Training <i class="glyphicon glyphicon-arrow-right"></i></a>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="modal-container-2" role="dialog" aria-labelledby="myModalLabels" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <center>
                    <h4 class="modal-title" id="myModalLabels">

                    </h4>
                </center>
            </div>
            <div class="modal-body">
                <b>Hasil :</b> <p id="hasil2"></p>
                <hr />
                <b>Keterangan :</b> <p id="keterangan2"></p>
                <hr />
                <b>Tujuan :</b> <p id="tujuan2"></p>
                <b>Target :</b> <p id="target2"></p>
            </div>
        </div>

    </div>
</div>


<script>
    var md = $('#modal-container-244164');
    function modalShowJadwal(mine) {
        $.ajax({
            type: "POST",
            url: "<?= Yii::app()->getBaseUrl(true) ?>/site/index",
            data: {"idj": mine.id},
            success: function (data) {
                $('#myModalLabel').html(data.nama);
                $('#jenis').html('Training ' + data.jenis);
                $('#kuota').html('Kuota ' + data.kuota + ' Peserta');
                $('#tujuan').html(data.tujuan);
                $('#target').html(data.target);
                $('#keterangan').html(data.keterangan);
                $('#follow').attr('href','<?=Yii::app()->getBaseUrl(true)?>/site/follow/'+data.id);
                md.modal('show');
            }, error: function () {
                alert("error json");
            },
            dataType: "json"
        });
    }

    function follow(mine) {
        $.ajax({
            type: "POST",
            url: "<?= Yii::app()->getBaseUrl(true) ?>/site/index",
            data: {"idj": mine.id},
            success: function (data) {
                $('#myModalLabel').html(data.nama);
                $('#jenis').html('Training ' + data.jenis);
                $('#tujuan').html(data.tujuan);
                $('#target').html(data.target);
                md.modal('show');
            }, error: function () {
                alert("error json");
            },
            dataType: "json"
        });
    }
    
    
    var md2 = $('#modal-container-2');
    function modalShow(mine) {
        $.ajax({
            type: "POST",
            url: "<?= Yii::app()->getBaseUrl(true) ?>/site/index",
            data: {"idt": mine.id},
            success: function (data) {
                $('#myModalLabels').html(data.nama);
                $('#hasil2').html(data.hasil);
                $('#kuota2').html('Kuota ' + data.kuota + ' Peserta');
                $('#tujuan2').html(data.tujuan);
                $('#target2').html(data.target);
                $('#keterangan2').html(data.keterangan);
                md2.modal('show');
            }, error: function () {
                alert("error json");
            },
            dataType: "json"
        });
    }
</script>

