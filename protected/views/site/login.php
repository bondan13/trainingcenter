<div class="col-lg-4 col-lg-offset-4" style="margin-top: 100px;">
    
    <div class="form">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'login-form',
            'enableClientValidation' => true,
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
        ));
        ?>

        <div class="row">
            <?php echo $form->labelEx($model, 'username'); ?><?php echo $form->error($model, 'username'); ?>
            <?php echo $form->textField($model, 'username', ['class' => 'form-control']); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'password'); ?><?php echo $form->error($model, 'password'); ?>
            <?php echo $form->passwordField($model, 'password', ['class' => 'form-control']); ?>
        </div>
        <br />
        <?php echo $form->error($model, 'authenticate'); ?>
        <div class="row buttons">
            <?php echo CHtml::submitButton('Login',['class'=>'btn btn-success btn-block']); ?>
        </div>

        <?php $this->endWidget(); ?>
    </div><!-- form -->
</div>
