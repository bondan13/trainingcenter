<?php
$baseUrl = Yii::app()->getBaseUrl(true);
$cs = Yii::app()->clientScript;
$cs->registerCssFile($baseUrl . '/bootstrap/css/bootstrap.css')
        ->registerCoreScript('jquery')
        ->registerScriptFile($baseUrl . '/bootstrap/js/bootstrap.min.js', CClientScript::POS_END);

?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="language" content="en">
        <title>P.T Gajah Tunggal Training Center </title>
    </head>

    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <?= $content ?>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <img src="<?=Yii::app()->getBaseUrl(true)?>/images/gt.gif" height="50" />
                <img src="<?=Yii::app()->getBaseUrl(true)?>/images/gt-radial.jpg" height="50" />
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="#">
                            <?php if(!Yii::app()->user->isGuest){ ?>
                            <i class="glyphicon glyphicon-user"></i> <?=Yii::app()->user->getState('nama')?>
                            <?php }else{?>
                            Training center
                            <?php }?>
                        </a>
                    </li>
                </ul>
                <?php if(!Yii::app()->user->isGuest){ ?>
                <ul class="nav navbar-nav navbar-right">
                    <li class="active">
                        <a href="<?=Yii::app()->getBaseUrl(true)?>/site/logout"> LOGOUT <i class="glyphicon glyphicon-new-window"></i></a>
                    </li>
                </ul>
                <?php }?>
            </div>

        </nav>
    </body>
</html>
