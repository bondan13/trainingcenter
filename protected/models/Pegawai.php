<?php

/**
 * This is the model class for table "pegawai".
 *
 * The followings are the available columns in table 'pegawai':
 * @property string $id
 * @property string $nip
 * @property string $nama
 * @property string $departemen_id
 * @property integer $level_id
 */
class Pegawai extends CActiveRecord {

    public $departemen_nama;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'pegawai';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('nip, nama, departemen_id, level_id', 'required'),
            array('nip', 'unique', 'message' => 'Nip sudah ada yang punya'),
            array('level_id', 'numerical', 'integerOnly' => true),
            array('nip', 'length', 'max' => 7),
            array('nama', 'length', 'max' => 255),
            array('departemen_id', 'length', 'max' => 10),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, nip, nama, departemen_id, departemen_nama, level_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'departemen' => array(self::BELONGS_TO, 'Departemen', 'departemen_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'nip' => 'Nip',
            'nama' => 'Nama',
            'departemen_id' => 'Departemen',
            'level_id' => 'Level',
            'departemen_nama' => 'Departemen',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->with = array('departemen');
        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.nip', $this->nip, true);
        $criteria->compare('t.nama', $this->nama, true);
        $criteria->compare('departemen.nama', $this->departemen_nama, true);
        $criteria->compare('t.level_id', $this->level_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'attributes' => array(
                    'id', true,
                    'nama', true,
                    'departemen_nama' => array(
                        'asc' => 'departemen.nama',
                        'desc' => 'departemen.nama DESC',
                    ),
                    'nip', true,
                    'level_id', true,
                ),
                'defaultOrder' => 't.id DESC'
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Pegawai the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
