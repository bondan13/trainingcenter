<?php

/**
 * This is the model class for table "training".
 *
 * The followings are the available columns in table 'training':
 * @property string $id
 * @property string $pegawai_id
 * @property string $jadwal_id
 * @property string $status
 * @property string $hasil
 * @property string $keterangan
 * @property string $training_id
 */
class Training extends CActiveRecord {

    public $pegawai_nama;
    public $pegawai_nip;
    public $jadwal_tanggal;
    public $training_nama;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'training';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('pegawai_id, jadwal_id, status, training_id', 'required'),
            array('pegawai_id, jadwal_id, status, training_id', 'length', 'max' => 10),
            array('hasil', 'length', 'max' => 255),
            array('keterangan', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, pegawai_nip, pegawai_id, jadwal_tanggal, jadwal_id, status, hasil, keterangan, pegawai_nama, training_nama, training_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'pegawai' => array(self::BELONGS_TO, 'Pegawai', 'pegawai_id', 'with' => 'departemen'),
            'jadwal' => array(self::BELONGS_TO, 'Jadwal', 'jadwal_id'),
            'itemtraining' => array(self::BELONGS_TO, 'ItemTraining', 'training_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'pegawai_id' => 'Pegawai',
            'jadwal_id' => 'Jadwal',
            'status' => 'Status',
            'hasil' => 'Hasil',
            'keterangan' => 'Keterangan',
            'training_id' => 'Training',
            'pegawai_nip' => 'NIP',
            'pegawai_nama' => 'Nama',
            'training_nama' => 'Training',
            'jadwal_tanggal' => 'Tanggal'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->with = array('pegawai', 'itemtraining', 'jadwal');
        $criteria->compare('pegawai.nip', $this->pegawai_nip, true);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('pegawai.nama', $this->pegawai_nama, true);
        $criteria->compare('jadwal.batas_pendaftaran', $this->jadwal_tanggal, true);
        $criteria->compare('itemtraining.nama', $this->training_nama, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'attributes' => array(
                    'jadwal_tanggal' => array(
                        'asc' => 'jadwal.batas_pendaftaran',
                        'desc' => 'jadwal.batas_pendaftaran DESC',
                    ),
                    'pegawai_nip' => array(
                        'asc' => 'pegawai.nip',
                        'desc' => 'pegawai.nip DESC',
                    ),
                    'pegawai_nama' => array(
                        'asc' => 'pegawai.nama',
                        'desc' => 'pegawai.nama DESC',
                    ),
                    'training_nama' => array(
                        'asc' => 'itemtraining.nama',
                        'desc' => 'itemtraining.nama DESC',
                    ), 'status'
                ),
                'defaultOrder' => 't.id DESC'
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Training the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function status() {
        $a = '';
        if ($this->status == 1) {
            $a = 'Pending';
        }
        if ($this->status == 2) {
            $a = 'Dijadwalkan';
        }
        if ($this->status == 3) {
            $a = 'Lulus';
        }
        if ($this->status == 4) {
            $a = 'Gagal';
        }
        return $a;
    }

}
