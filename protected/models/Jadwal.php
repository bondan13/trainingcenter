<?php

/**
 * This is the model class for table "jadwal".
 *
 * The followings are the available columns in table 'jadwal':
 * @property string $id
 * @property string $training_id
 * @property string $batas_pendaftaran
 * @property integer $kuota
 * @property string $keterangan
 * @property integer $status
 */
class Jadwal extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'jadwal';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('training_id, batas_pendaftaran, kuota, status', 'required'),
            array('kuota, status', 'numerical', 'integerOnly' => true),
            array('training_id', 'length', 'max' => 10),
            array('keterangan', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, training_id, itemtraining, batas_pendaftaran, kuota, keterangan, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'itemtraining' => array(self::BELONGS_TO, 'ItemTraining', 'training_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'training_id' => 'Training',
            'batas_pendaftaran' => 'Batas Pendaftaran',
            'kuota' => 'Kuota',
            'keterangan' => 'Keterangan',
            'status' => 'Status',
            'itemtraining' => 'Training'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;
        $criteria->with = ['itemtraining'];
        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.status', $this->status);
        $criteria->compare('itemtraining.nama', $this->itemtraining,true);
        

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'attributes' => array(
                    'itemtraining' => array(
                        'asc' => 'itemtraining.nama',
                        'desc' => 'itemtraining.nama DESC',
                    ),
                    'id',
                    'status'
                ),
                'defaultOrder' => 't.id DESC'
            )
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Jadwal the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

}
