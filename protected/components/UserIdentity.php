<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
//        $users = array(
//            // username => password
//            'admin' => 'admin',
//        );
        
        $user = Pegawai::model()->findByAttributes(['nip' => $this->username]);
        
        if($user){
            if($user->nip == $this->password){
                $this->errorCode = self::ERROR_NONE;
                Yii::app()->user->setState('nama', $user->nama);
                Yii::app()->user->setState('level', $user->level_id);
                Yii::app()->user->setState('id', $user->id);
            }else{
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
            }
        }elseif($this->username == 'admin' && $this->password == 'admin'){
            $this->errorCode = self::ERROR_NONE;
            Yii::app()->user->setState('nama', 'Admin');
        }else{
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        }
        
        return !$this->errorCode;
    }

}
