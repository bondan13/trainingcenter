<?php

class TrainingController extends AdminController {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'admin', 'delete', 'create', 'update', 'laporan'),
                'users' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Training;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Training'])) {
            $model->attributes = $_POST['Training'];
            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionLaporan() {
        $ds = @$_POST['Report']['date_start'];
        $dn = @$_POST['Report']['date_end'];
        $l = @$_POST['listtraining'];
        $s = @$_POST['status'];
        $listtraining = ItemTraining::model()->findAll();
        if (($ds) && ($dn)) {
            if ($ds > $dn) {
                $message = 'Tanggal akhir harus lebih besar';
            } else {
                $criteria = new CDbCriteria();
                $criteria->with = array('jadwal', 'pegawai', 'itemtraining');
                $criteria->addBetweenCondition('jadwal.batas_pendaftaran', $_POST['Report']['date_start'], $_POST['Report']['date_end']);
                if (@$_POST['listtraining'] > 0) {
                    $criteria->compare('t.training_id', $l);
                }
                if (@$_POST['status'] > 0) {
                    $criteria->compare('t.status', $s);
                }
                $criteria->order = 't.training_id ASC, t.status ASC';
                $training = Training::model()->findAll($criteria);

                $header = array(
                    array('label' => 'Tanggal', 'length' => '20', 'align' => 'C'),
                    array('label' => 'NIP', 'length' => '15', 'align' => 'C'),
                    array('label' => 'Nama', 'length' => '70', 'align' => 'C'),
                    array('label' => 'Departemen', 'length' => '40', 'align' => 'C'),
                    array('label' => 'Training', 'length' => '110', 'align' => 'C'),
                    array('label' => 'Status', 'length' => '20', 'align' => 'C'),
                );
                $this->renderPartial('laporan', ['judul' => 'Laporan Training Pegawai', 'date' => $_POST, 'header' => $header, 'training' => $training]);
            }
        }
        $this->render('report', ['message' => @$message, 'listtraining' => $listtraining]);
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Training'])) {
            $model->attributes = $_POST['Training'];
            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionIndex() {
        $model = new Training('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Training']))
            $model->attributes = $_GET['Training'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Training the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Training::model()->with('pegawai', 'jadwal', 'itemtraining')->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Training $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'training-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
