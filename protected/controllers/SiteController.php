<?php

class SiteController extends Controller {

    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('login', 'error'),
                'users' => array('*'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'logout', 'follow'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        if (strtolower(@Yii::app()->user->getState('nama')) == 'admin') {
            $this->redirect(Yii::app()->getBaseUrl(true) . '/admin/pegawai');
        }
        if (isset($_POST['idj'])) {
            $id = $_POST['idj'];
            $t = Jadwal::model()->with('itemtraining')->findByPk($id);
            header('Content-Type: application/json');
            echo json_encode($t->attributes+$t->itemtraining->attributes);
            exit();
        }
        if (isset($_POST['idt'])) {
            $id = $_POST['idt'];
            $t = Training::model()->with('itemtraining','jadwal')->findByPk($id);
            header('Content-Type: application/json');
            echo json_encode($t->attributes+$t->itemtraining->attributes+$t->jadwal->attributes);
            exit();
        }
        $ids = [];
        $c = new CDbCriteria();
        $c->with = ['itemtraining', 'jadwal'];
        $c->compare('t.pegawai_id', Yii::app()->user->getState('id'));
        $c->order = 't.status ASC';
        $training = Training::model()->findAll($c);
        if ($training) {
            foreach ($training as $t) {
                if ($t->status != 4) {
                    $ids[] = $t->training_id;
                }
            }
        }

        $criteria = new CDbCriteria();
        $criteria->with = ['itemtraining'];
        $criteria->addCondition('itemtraining.level <= ' . Yii::app()->user->getState('level'));
        $criteria->addCondition('t.batas_pendaftaran >= NOW()');
        $criteria->addNotInCondition('itemtraining.id', $ids);        
        $criteria->compare('t.status', 1);
        $itraining = Jadwal::model()->findAll($criteria);

        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('index', ['training' => $training, 'itraining' => $itraining]);
    }

    public function actionFollow($id) {
        
        $jadwal = Jadwal::model()->findByPk($id);
        
        if($jadwal){
            $training = new Training();
            $training->jadwal_id = $id;
            $training->training_id = $jadwal->training_id;
            $training->pegawai_id = Yii::app()->user->getState('id');
            $training->status = 1;
            $training->save();
        }
        
        $this->redirect(Yii::app()->getBaseUrl(true));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {

        if (!Yii::app()->user->isGuest) {
            $this->redirect('index');
        }
        $model = new LoginForm;

        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                if ($model->username == 'admin') {
                    $this->redirect(Yii::app()->getBaseUrl(true) . '/admin/pegawai');
                } else {
                    $this->redirect('index');
                }
            }
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

}
